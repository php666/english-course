<!doctype html>
<html lang="pl">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="<?php echo site_url(); ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo site_url(); ?>css/style.css">
        <script src="<?php echo site_url(); ?>js/jquery-3.5.1.min.js"></script>
        <script src="<?php echo site_url(); ?>js/bootstrap.min.js"></script>
        <script type="text/javascript">
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 10000);
        </script>
        <script type="text/javascript">
            $(window).on('load', function () {
                $('#loginModal').modal({
                    backdrop: 'static',
                    keyboard: false,
                    'show': true
                });
                setTimeout(function (){
                    $('#email').focus();
                }, 1000);
            });
        </script>
        <style>
            .modal-backdrop {
                background-color: #ecf0f1;
            }
        </style>
        <title>Strona logowania</title>
    </head>

    <body style="background-color: #FFF;">

        <div id="loginModal" class="modal fade">
            <div class="modal-dialog modal-login shadow p-2 mb-4 bg-white rounded">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="left">

                        </p>
                        <p class="right">
                            
                        </p>
                    </div>
                    <div class="modal-body">

                        <form action="<?php echo site_url(); ?>" method="POST">
                            <div class="form-group">
                                <i class="fa fa-user"></i>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Adres e-mail" required="required">
                            </div>
                            <div class="form-group">
                                <i class="fa fa-lock"></i>
                                <input type="password" name="password" class="form-control" placeholder="Hasło" required="required">					
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-outline-secondary btn-sm" value="Zaloguj się">
                            </div>
                        </form>
                        
                    </div>
                    <div class="modal-footer">
                        footer
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>